var express = require('express');
var router = express.Router();
var db = require('../controllers/database.js');
/* GET home page. */
router.get("/api/trades/users/:id", (req, res, next) => {

    var sql = "select * from stock_trade_user where user = " + req.params.id + "";
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({"error": err.message});
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });

});

router.get("/api/trades/stocks/:symbol", (req, res, next) => {

    var sql = "select * from stock_trade_user where symbol = " + req.params.symbol + "";
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({"error": err.message});
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });

});

router.get("/api/trades/users/:id/stocks/:symbol", (req, res, next) => {

    var sql = "select * from stock_trade_user where symbol = " + req.params.symbol + " && user = " + req.params.id + "";
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({"error": err.message});
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });

});


router.post("/api/trades", (req, res, next) => {
    var errors = []
    if (!req.body.type) {
        errors.push("No type specified");
    }
    if (!req.body.user) {
        errors.push("No user specified");
    }
    if (!req.body.symbol) {
        errors.push("No symbol specified");
    }
    if (!req.body.shares) {
        errors.push("No shares specified");
    }
    if (!req.body.price) {
        errors.push("No price specified");
    }
    if (errors.length) {
        res.status(400).json({"error": errors.join(",")});
        return;
    }
    var data = {
        type: req.body.type,
        user: req.body.user,
        symbol: req.body.symbol,
        shares: req.body.shares,
        price: req.body.price,
    }
    var sql = 'INSERT INTO stock_trade_user (type, user, symbol, shares, price) VALUES (?,?,?,?,?)';
    var params = [data.type, data.user, data.symbol, data.shares, data.price]
    db.run(sql, params, function (err, result) {
        if (err) {
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id": this.lastID
        })
    });
})
module.exports = router;
