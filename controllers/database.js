var sqlite3 = require('sqlite3').verbose();

var md5 = require('md5');

const DBSOURCE = "db.sqlite";

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message);
        throw err
    }else{

        db.run(`CREATE TABLE stock_trade_user (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            type text, 
            user text , 
            symbol text, 
            shares text, 
            price text, 
            timestamp datetime DEFAULT CURRENT_TIMESTAMP
            );`,
            (err) => {
                if (err) {
                    // Table already created
                    console.log(err);
                    var insert = 'INSERT INTO stock_trade_user (type, user, symbol, shares, price) VALUES (?,?,?,?,?)'
                    db.run(insert, ["chaman","1","abc", "shares", "price"])
                }else{
                    // Table just created, creating some rows
                    var insert = 'INSERT INTO stock_trade_user (type, user, symbol, shares, price) VALUES (?,?,?,?,?,?)'
                    db.run(insert, ["chaman","admin@example.com","abc", "shares", "price"])
                }
            });
    }
});

module.exports = db;